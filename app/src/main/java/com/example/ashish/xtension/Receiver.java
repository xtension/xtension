package com.example.ashish.xtension;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ResourceBundle;

public class Receiver extends Thread {

    private final String ERROR_LOG = "ERROR";

    protected BluetoothSocket mSocket;

    protected InputStream mInStream;

    protected OutputStream mOutStream;

    protected ControllerSide side;

    protected int jMode;

    public Receiver(BluetoothSocket socket, ControllerSide side) {
        this.mSocket = socket;
        this.side = side;

        if (side == ControllerSide.LEFT)
            jMode = JoystickMode.JOYSTICK_UP;
        else if (side == ControllerSide.RIGHT)
            jMode = JoystickMode.JOYSTICK_DOWN;
        else {
            jMode = JoystickMode.JOYSTICK_LEFT;
        }

        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        // Get the input and output streams; using temp objects because
        // member streams are final.
        try {
            tmpIn = mSocket.getInputStream();
        } catch (IOException e) {
            Log.e(ERROR_LOG, "Error occurred when creating input stream", e);
        }
        try {
            tmpOut = mSocket.getOutputStream();
        } catch (IOException e) {
            Log.e(ERROR_LOG, "Error occurred when creating output stream", e);
        }

        mInStream = tmpIn;
        mOutStream = tmpOut;
    }

    @Override
    public void run() {
    }

    protected void write(byte[] b) throws IOException {
        Log.d("Send Receiver to the " + side + " Controller:", java.util.Arrays.toString(b));
        mOutStream.write(b);
    }

    protected void setSide(ControllerSide side){
        this.side = side;
    }

    protected ControllerSide getSide(){
        return side;
    }

    protected void setJMode(int jMode) {
        this.jMode = jMode;
    }

    protected void cancel() {
        interrupt();
    }

}
