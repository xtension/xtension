package com.example.ashish.xtension;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import java.io.Serializable;

public class AnimationActivity extends AppCompatActivity   {

    private ImageView logo;
    private ImageView text;

    private AnimationSet logoSet;
    private AnimationSet textSet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);
        getSupportActionBar().hide();

        //start Methods
        //hideStatusBar();
        setAnimation();

        text = findViewById(R.id.text);
        text.setVisibility(View.INVISIBLE);

        logo = findViewById(R.id.logo);
        logo.startAnimation(logoSet);


        logoSet.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                logoSet.setFillAfter(true);
                text.startAnimation(textSet);
                textSet.setFillAfter(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        textSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Intent intent = new Intent(AnimationActivity.this, HomeActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.transition_on,R.anim.transition_off);
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    @Override
    public void finish() {
        super.finish();
    }

    //Hide the status bar + Fullscreen, because of the logo
    private void hideStatusBar() {
        getSupportActionBar().hide();
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    //Set the Animations and add them in a list in correct order (AnimationSet)
    private void setAnimation() {
        logoSet = new AnimationSet(false);
        textSet = new AnimationSet(false);
        final Animation scale = AnimationUtils.loadAnimation(this, R.anim.scale);
        final Animation transition = AnimationUtils.loadAnimation(this, R.anim.blink_logo);
        final Animation text_blink = AnimationUtils.loadAnimation(this, R.anim.blink_text);


        logoSet.addAnimation(scale);
        logoSet.addAnimation(transition);
        textSet.addAnimation(text_blink);
    }
}
