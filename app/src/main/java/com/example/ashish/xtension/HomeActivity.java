package com.example.ashish.xtension;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.ImageButton;
import android.widget.ImageView;
public class HomeActivity extends AppCompatActivity  {

    private ImageView title;

    private ImageButton start;
    private ImageButton info;

    private AnimationSet blinkSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();


        title = findViewById(R.id.title);
        setTitleAnimation();
        title.startAnimation(blinkSet);
        blinkSet.setFillAfter(true);
        start = findViewById(R.id.startButton);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActivity(BluetoothActivity.class, R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
        info = findViewById(R.id.infoButton);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActivity(InfoActivity.class,R.anim.slide_in_right,R.anim.slide_out_left);
            }
        });
    }

    public void finishActivity(Class classPicked, int resource1, int resource2) {
        Intent intent = new Intent(HomeActivity.this, classPicked);
        startActivity(intent);
        overridePendingTransition(resource1, resource2);
        finish();
    }

    private void setTitleAnimation() {
        blinkSet = new AnimationSet(false);
        AlphaAnimation fadeIn = new AlphaAnimation(0.5f, 1.0f);
        AlphaAnimation fadeOut = new AlphaAnimation(1.0f, 0.5f);
        fadeIn.setDuration(2000);
        fadeOut.setDuration(2000);
        fadeOut.setStartOffset(2000);
        blinkSet.addAnimation(fadeIn);
        blinkSet.addAnimation(fadeOut);
        blinkSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //repeat
                title.startAnimation(blinkSet);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}
