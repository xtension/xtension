package com.example.ashish.xtension;

public interface JoystickMode {
    int JOYSTICK_LEFT = 0; //Default
    int JOYSTICK_UP = 1;
    int JOYSTICK_RIGHT = 2;
    int JOYSTICK_DOWN = 3;
}
