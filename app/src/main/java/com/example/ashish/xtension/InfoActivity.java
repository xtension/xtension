package com.example.ashish.xtension;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;

public class InfoActivity extends AppCompatActivity {

    private ImageView scroll;
    private AnimationSet transitionSet;
    private ImageButton backButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        getSupportActionBar().hide();


        scroll = findViewById(R.id.scroll_imageView);
        backButton = findViewById(R.id.backbutton_info);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActivity(HomeActivity.class, R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });

        setAnimation();
        scroll.startAnimation(transitionSet);
        transitionSet.setFillAfter(true);
    }

    @Override
    public void onBackPressed() {
        // executeScan(ScanState.SCAN_BACK);
        finishActivity(HomeActivity.class, R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public void finishActivity(Class classPicked, int resource1, int resource2) {
        Intent intent = new Intent(InfoActivity.this, classPicked);
        startActivity(intent);
        overridePendingTransition(resource1, resource2);
        finish();
    }

    private void setAnimation(){
        transitionSet = new AnimationSet(false);
        final Animation transition = AnimationUtils.loadAnimation(this, R.anim.transition_on);

        transitionSet.addAnimation(transition);

    }
}
