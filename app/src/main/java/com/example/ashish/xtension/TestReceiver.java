package com.example.ashish.xtension;


import android.bluetooth.BluetoothSocket;
import android.os.Handler;

import android.os.Message;
import android.util.Log;

import java.io.IOException;


public class TestReceiver extends Receiver {

    private static final int STANDARD_X_VALUE = 128;
    private static final int STANDARD_Y_VALUE = 128;
    private static final int X_MINIMUM_VALUE = 0, Y_MINIMUM_VALUE = 0;
    private static final int X_MAXIMUM_VALUE = 255;
    private static final int Y_MAXIMUM_VALUE = 255;

    private static final double X_FACTOR = 0.95;
    private static final double Y_FACTOR = 0.95;
    private static final double X_OFFSET = STANDARD_X_VALUE * X_FACTOR;
    private static final double Y_OFFSET = STANDARD_Y_VALUE * Y_FACTOR;


    private final String TEST_ACTIVITY = "Test Activity";

    private int[] buttonsData = new int[8];

    private byte[] mBuffer = new byte[1024]; // mBuffer store for the strea

    private int bufferMessage = MessageConstants.MESSAGE_DEFAULT;
    private int numBytes;

    private int xValue, yValue;

    private int yJoystickPoint = 0;
    private int xJoystickPoint = 0;

    private boolean bufferState = true;
    private boolean bBool, xBool, yBool;


    public TestReceiver(BluetoothSocket socket, ControllerSide side) {
        super(socket, side);
    }

    @Override
    public void run() {
        Log.d(TEST_ACTIVITY, "Running...");
        while (!this.isInterrupted()) {
            try {
                //read from the buffer
                numBytes = mInStream.read(mBuffer, 0, 1024);
                //Log.d("TEST", java.util.Arrays.toString(mBuffer));
                for (int i = 0; i < numBytes; i++) {
                    if (bufferState) {
                        bufferState = false;
                        switch (mBuffer[i]) {
                            case 'B':
                                bufferMessage = MessageConstants.MESSAGE_B;
                                break;
                            case 'X':
                                bufferMessage = MessageConstants.MESSAGE_X;
                                break;
                            case 'Y':
                                bufferMessage = MessageConstants.MESSAGE_Y;
                                break;
                            default:
                                bufferMessage = MessageConstants.MESSAGE_DEFAULT;
                                bufferState = true;
                                break;
                        }
                    } else {
                        bufferState = true;
                        doAction(bufferMessage, mBuffer[i]);
                    }
                }
            } catch (Exception e) {
                Log.d(TEST_ACTIVITY, "Input stream was disconnected", e);
                ItemListActivity.errorSaver.setErrorState(true);
                cancel();
            }
        }
        try {
            mSocket.close();
            mInStream.close();
            mOutStream.close();
            Log.d(TEST_ACTIVITY, "Closing Socket and Stream.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void doAction(int buffMsg, byte data) {
        int dataByte = bytesToInt(data);

        switch (buffMsg) {
            case MessageConstants.MESSAGE_B:
                splitButtons(data);
                bBool = true;
                break;
            case MessageConstants.MESSAGE_X:
                xValue = dataByte;
                xBool = true;
                break;
            case MessageConstants.MESSAGE_Y:
                yValue = dataByte;
                yBool = true;
                break;
        }

        if (bBool && xBool && yBool) {
            //Log.d("Status: ", "End at " + count);
            outOfBounds();
            setMode(buttonsData, xValue, yValue, jMode);
        }
    }

    private synchronized void setMode(int[] buttonsData, int xValueSide, int yValueSide, int jMode) {
        int xValue = (int) ((xValueSide * X_FACTOR) - X_OFFSET);
        int yValue = (int) ((yValueSide * Y_FACTOR) - Y_OFFSET);

        switch (jMode) {
            case JoystickMode.JOYSTICK_LEFT:
                setValues(buttonsData, xValue, yValue, 1, 1);
                break;
            case JoystickMode.JOYSTICK_UP:
                setValues(buttonsData, yValue, xValue, -1, 1);
                break;
            case JoystickMode.JOYSTICK_RIGHT:
                setValues(buttonsData, xValue, yValue, -1, -1);
                break;
            case JoystickMode.JOYSTICK_DOWN:
                setValues(buttonsData, yValue, xValue, 1, -1);
                break;
        }
    }

    private synchronized void setValues(int[] buttonsData, int valueOne, int valueTwo, int signX, int signY) {

        Message msg = new Message();
        int xDir;
        int yDir;

        xDir = valueOne - xJoystickPoint;
        yDir = valueTwo - yJoystickPoint;
        xJoystickPoint = valueOne;
        yJoystickPoint = valueTwo;

        int currXPosition = (signX * xDir);
        int currYPosition = (signY * yDir);

        msg.obj = buttonsData;
        msg.arg1 = currXPosition;
        msg.arg2 = currYPosition;

        bBool = xBool = yBool = false;

        sendHandler(msg);
    }

    private synchronized void splitButtons(byte dataByte) {
        for (int i = buttonsData.length - 1; i >= 0; i--) {
            if ((dataByte & (1 << i)) != 0)
                buttonsData[i] = 1;
            else
                buttonsData[i] = 0;
        }
    }

    private void sendHandler(Message msg) {
        if (checkIfHandlerExists(ControllerSide.LEFT, ItemListActivity.mHandler_left)) {
            ItemListActivity.mHandler_left.sendMessage(msg);
        }
        if (checkIfHandlerExists(ControllerSide.RIGHT, ItemListActivity.mHandler_right)) {
            ItemListActivity.mHandler_right.sendMessage(msg);
        }
    }

    private boolean checkIfHandlerExists(ControllerSide checkSide, Handler mHandler) {
        return side == checkSide && mHandler != null;
    }

    private void outOfBounds() {
        if (xValue < X_MINIMUM_VALUE)
            xValue = X_MINIMUM_VALUE;
        else if (xValue > X_MAXIMUM_VALUE)
            xValue = X_MAXIMUM_VALUE;

        if (yValue < Y_MINIMUM_VALUE)
            yValue = Y_MINIMUM_VALUE;
        else if (yValue > Y_MAXIMUM_VALUE)
            yValue = Y_MAXIMUM_VALUE;
    }


    private int bytesToInt(byte bytes) {
        return (bytes & 0xFF);
    }


}
