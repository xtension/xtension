package com.example.ashish.xtension;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

public class BluetoothActivity extends AppCompatActivity {

    //public
    public static final String BLUETOOTH_DEVICE = "BT-Device:";
    public static final String MAIN_ACTIVITY = "Main Activity";

    public static ArrayList<TestReceiver> testReceiverArrayList;
    public static ArrayList<BluetoothSocket> mBTSocketDeviceList;
    public static ArrayList<ControllerSide> sideArrayList;

    public final String BLUETOOTH_ACTIVITY_TAG = "BluetoothActivity";

    public static boolean errorState = false;
    public static boolean socketState = false;

    //private
    private final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private final int MAX_DEVICES_CONNECTED = 1;
    private final int REQUEST_CODE_BLUETOOTH = 1;

    private static int count = 0;

    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


    //Resources
    private ImageButton button_OnOff;
    private ImageButton button_scan;
    private ImageButton backButton;

    private TextView scan_textView;

    private Runnable scanRunnable;

    private Handler mScanHandler;

    private Intent enableBluetooth;
    private BluetoothDevice mBTDevice;

    private AlertDialog dialog;

    private ListView mListView;

    private ConnectThread t;

    private ControllerSide side = ControllerSide.LEFT;

    private ArrayList<String> mBTStringDeviceList;
    private ArrayList<BluetoothDevice> mBTDeviceList;
    private ArrayAdapter<BluetoothDevice> mArrayAdapter;

    private int animationCounter = 1;

    private boolean scanAnimationState = false;
    private boolean scanStatus = false;

    private String[] scanString = {"SCAN", "SCANN", "SCANNI", "SCANNIN", "SCANNING", "SCANNING.", "SCANNING..", "SCANNING..."};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
        getSupportActionBar().hide();

        testReceiverArrayList = new ArrayList<>();

        scan_textView = findViewById(R.id.scan_text);
        button_OnOff = findViewById(R.id.button_onoff);
        button_scan = findViewById(R.id.button_scan);

        backButton = findViewById(R.id.backbutton_bluetooth);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeScan(ScanState.SCAN_STOP);
                finishActivity(HomeActivity.class, R.anim.slide_in_left, R.anim.slide_out_right);
            }
        });
        mListView = findViewById(R.id.listview_itemlist);

        mBTStringDeviceList = new ArrayList<>();
        mBTDeviceList = new ArrayList<>();
        sideArrayList = new ArrayList<>();
        mBTSocketDeviceList = new ArrayList<>();


        //ArrayAdapter(Context context, int resource, int textViewResourceId, T[] objects)
        mArrayAdapter = new ArrayAdapter(getApplicationContext(), R.layout.layout_ownlistview, R.id.device_listview, mBTStringDeviceList);
        mListView.setAdapter(mArrayAdapter);

        checkButtonState();
        checkButtonScan();
        checkItemList();


        //set IntentFilter to catch Actions in @mBluetoothAdapterReceiver
        IntentFilter discoveryDevicesIntent = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mBluetoothAdapterReceiver, discoveryDevicesIntent);

        IntentFilter mBluetoothDeviceFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mBluetoothDeviceAdapter, mBluetoothDeviceFilter);

        //if true show error alert
        if (errorState) {
            setDisconnectedDialog(R.layout.layout_disconnected_dialog);
            executeScan(ScanState.SCAN_STOP);
        }


    }

    private void openItemListActivity() {
        Intent openItemListActivity = new Intent(BluetoothActivity.this, ItemListActivity.class);
        t.interrupt();
        startActivity(openItemListActivity);
        finish();
    }

    //check first if bluetooth is enabled and set afterwards the image
    private void checkButtonState() {
        if (mBluetoothAdapter.isEnabled()) {
            button_scan.setVisibility(View.VISIBLE);
            scan_textView.setVisibility(View.VISIBLE);
            button_OnOff.setBackgroundResource(R.drawable.button_on);
        } else {
            button_scan.setVisibility(View.INVISIBLE);
            scan_textView.setVisibility(View.INVISIBLE);
            button_OnOff.setBackgroundResource(R.drawable.button_off);
        }

        button_OnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBluetoothAdapter.isEnabled()) {
                    mBluetoothAdapter.disable();
                    button_scan.setVisibility(View.INVISIBLE);
                    scan_textView.setVisibility(View.INVISIBLE);
                    button_OnOff.setBackgroundResource(R.drawable.button_off);
                } else {
                    enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBluetooth, REQUEST_CODE_BLUETOOTH);
                }
                //execute
                executeScan(ScanState.SCAN_STOP);
            }
        });
    }

    @Override
    public void onBackPressed() {
        executeScan(ScanState.SCAN_STOP);
        finishActivity(HomeActivity.class, R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void finishActivity(Class classPicked, int resource1, int resource2) {
        Intent openActivity = new Intent(BluetoothActivity.this, classPicked);
        startActivity(openActivity);
        overridePendingTransition(resource1, resource2);
        finish();
    }


    private void checkButtonScan() {
        button_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getScanStatus()) {
                    setScanStatus(false);
                    executeScan(ScanState.SCAN_STOP);
                } else {
                    setScanStatus(true);
                    executeScan(ScanState.SCAN_START);
                }

            }
        });
    }


    private void executeScan(ScanState scanState) {

        switch (scanState) {

            case SCAN_START:
                cancelDiscovery();

                mBluetoothAdapter.startDiscovery();

                clearDevices();

                removeScanner();

                clearArrayAdapter();

                checkBTPermissions();

                startScanHandler();
                break;

            case SCAN_STOP:

                cancelDiscovery();

                resetScanText();

                clearDevices();

                removeScanner();

                clearArrayAdapter();
                break;

            case SCAN_FINISHED:
                resetScanText();

                removeScanner();

                break;
        }
    }

    private void cancelDiscovery() {
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }
    }

    private void clearDevices() {

        //1.Variante
        mBTDeviceList.clear();
        mBTStringDeviceList.clear();
        mBTSocketDeviceList.clear();
        sideArrayList.clear();

    }

    private void removeScanner() {
        if (scanRunnable != null)
            mScanHandler.removeCallbacks(scanRunnable);
    }

    private void clearArrayAdapter() {
        mArrayAdapter.clear();
        mArrayAdapter.notifyDataSetChanged();
    }

    private void resetScanText() {
        count = 0;
        animationCounter = 0;
        scanAnimationState = false;
        scan_textView.setGravity(View.TEXT_ALIGNMENT_GRAVITY);
        scan_textView.setText(scanString[animationCounter]);
    }


    private void startScanHandler() {

        //like runOnUiThread
        mScanHandler = new Handler();
        mScanHandler.post(scanRunnable = new Runnable() {
            @Override
            public void run() {
                if (!scanAnimationState) {

                    scan_textView.setText(scanString[animationCounter++]);

                    if (animationCounter == scanString.length)
                        animationCounter = 4;

                    mScanHandler.postDelayed(this, 400);
                }
            }
        });

    }


    public void setScanStatus(boolean status) {
        scanStatus = status;
    }

    public boolean getScanStatus() {
        return scanStatus;
    }


    private void setDisconnectedDialog(int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(BluetoothActivity.this);
        LayoutInflater inflater = BluetoothActivity.this.getLayoutInflater();
        View inflaterView = inflater.inflate(id, null);
        builder.setView(inflaterView);
        Button cancelButton = inflaterView.findViewById(R.id.cancelButton);
        builder.setCancelable(false);
        final AlertDialog alert = builder.create();
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorState = false;
                alert.dismiss();
            }
        });


        alert.show();
    }

    private void setDialog(int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(BluetoothActivity.this);
        LayoutInflater inflater = BluetoothActivity.this.getLayoutInflater();
        View inflaterView = inflater.inflate(id, null);
        builder.setView(inflaterView);
        builder.setCancelable(false);
        dialog = builder.create();
        dialog.show();
        ImageButton test_dialog = inflaterView.findViewById(R.id.test_dialog);
        test_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < MAX_DEVICES_CONNECTED; i++) {
                    TestReceiver mTestData = new TestReceiver(mBTSocketDeviceList.get(i), sideArrayList.get(i));
                    mTestData.start();
                    testReceiverArrayList.add(mTestData);
                }

                openItemListActivity();
                dialog.dismiss();
            }
        });

        ImageButton connect_dialog = inflaterView.findViewById(R.id.connect_dialog);
        connect_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent serviceIntent = new Intent(BluetoothActivity.this, ConnectService.class);
                // startService(serviceIntent);

                for (int i = 0; i < 1; i++) {
                    ConnectReceiver connectReceiver = new ConnectReceiver(BluetoothActivity.mBTSocketDeviceList.get(i), BluetoothActivity.sideArrayList.get(i));
                    connectReceiver.start();
                }

                SocketProvider unitySocket = null;
                unitySocket = new SocketProvider(5160);




                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startMain);

                dialog.dismiss();
            }
        });

    }


    //if(Version) > LOLLIPOP then check Permissions, otherwise it wont work
    private void checkBTPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck = this.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
            permissionCheck += this.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            if (permissionCheck != 0) {
                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
            }
        } else {
            Log.d(MAIN_ACTIVITY, "checkBTPermissions: No need to check permissions. SDK version < LOLLIPOP.");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_BLUETOOTH && mBluetoothAdapter.isEnabled()) {
            Log.d(BLUETOOTH_ACTIVITY_TAG, REQUEST_CODE_BLUETOOTH + "\n ");
            button_OnOff.setBackgroundResource(R.drawable.button_on);
            button_scan.setVisibility(View.VISIBLE);
            scan_textView.setVisibility(View.VISIBLE);
            //button_scan.setBackgroundResource(R.drawable.scan_zero);
        }
    }


    private final BroadcastReceiver mBluetoothAdapterReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {
                Log.d(BLUETOOTH_ACTIVITY_TAG, "Discovering finished...");
                setScanStatus(false);
                executeScan(ScanState.SCAN_FINISHED);

            }
        }
    };
    private final BroadcastReceiver mBluetoothDeviceAdapter = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                if (action.equals(BluetoothDevice.ACTION_FOUND)) {
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if ((device != null) && (device.getName() != null)) {
                        if (device.getName().equals("X1") || device.getName().equals("X2")) {
                            Log.d(BLUETOOTH_DEVICE, "Bluetooth Device found: " + device.getName());
                            String deviceInfo = device.getName() + "\n" + device.getAddress();
                            mBTStringDeviceList.add(deviceInfo);
                            mBTDeviceList.add(device);
                            mArrayAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBluetoothAdapterReceiver);
        unregisterReceiver(mBluetoothDeviceAdapter);
    }

    private void checkItemList() {
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mBluetoothAdapter.cancelDiscovery();
                mBTDevice = mBTDeviceList.get(position);
                if (!errorState) {
                    t = new ConnectThread(mBTDevice, side);
                    t.start();
                    side = side.other();
                }
            }
        });
    }


    private class ConnectThread extends Thread {

        public BluetoothDevice device;

        private BluetoothSocket mSocket;
        private ControllerSide side;


        private ConnectThread(BluetoothDevice device, ControllerSide side) {

            Log.d(BLUETOOTH_ACTIVITY_TAG, side.toString());
            BluetoothSocket tmp = null;
            this.device = device;
            this.side = side;

            try {
                tmp = this.device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                Log.e(BLUETOOTH_ACTIVITY_TAG, "Socket's create() method failed", e);
            }
            mSocket = tmp;
        }

        public void run() {
            mBluetoothAdapter.cancelDiscovery();
            try {
                //Log.d(BLUETOOTH_ACTIVITY_TAG, "Trying to connect...");
                mSocket.connect();
            } catch (IOException e) {
                cancel();
                e.printStackTrace();
            }
            if (mSocket.isConnected()) {
                Log.d(BLUETOOTH_ACTIVITY_TAG, "Connection successfully!");

                count++;
                mBTSocketDeviceList.add(mSocket);
                sideArrayList.add(side);

                BluetoothActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(BluetoothActivity.this, "Connected with the " + side.toString() + " Controller!", Toast.LENGTH_LONG).show();
                        setDialog(R.layout.layout_connecting_dialog);
                    }
                });
            }
        }

        private void cancel() {
            try {
                mSocket.close();
                mBTSocketDeviceList.clear();
            } catch (IOException e) {
                Log.e(BLUETOOTH_ACTIVITY_TAG, "Could not close the client socket", e);
            }
        }
    }
}