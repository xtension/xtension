package com.example.ashish.xtension;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.IOException;
import java.util.ArrayList;

public class ItemListActivity extends AppCompatActivity {

    public static Handler mHandler_left, mHandler_right;
    public static ErrorSaver errorSaver;

    private final String ONEPLAYER = "oneplayer";
    private final String TWOPLAYER = "twoplayer";


    private ImageButton[] buttons_left = new ImageButton[6];
    private ImageButton[] buttons_right = new ImageButton[6];

    private EditText[] vibration = new EditText[4];
    private final int VIBRATION_MAX = 255;
    private final int VIBRATION_MIN = 0;

    private Button leftVibration;
    private Button rightVibration;

    // array ([nix] [nix] [A] [D] [M] [I] [R/L] [J])
    private int[] buttonsLeftResources = {
            R.id.joystick_left,
            R.id.s_button_left,
            R.id.i_button_left,
            R.id.m_button_left,
            R.id.d_button_left,
            R.id.a_button_left,
    };

    private int[] buttonsRightResources = {
            R.id.joystick_right,
            R.id.s_button_right,
            R.id.i_button_right,
            R.id.m_button_right,
            R.id.d_button_right,
            R.id.a_button_right,
    };

    private int[] buttonsDrawable = {
            R.drawable.joystick_pressed,
            R.drawable.s_button_pressed,
            R.drawable.i_button_pressed,
            R.drawable.m_button_pressed,
            R.drawable.d_button_pressed,
            R.drawable.a_button_pressed,

            R.drawable.joystick_unpressed,
            R.drawable.s_button_unpressed,
            R.drawable.i_button_unpressed,
            R.drawable.m_button_unpressed,
            R.drawable.d_button_unpressed,
            R.drawable.a_button_unpressed
    };

    private int[] vibrationResources = new int[]{
            R.id.vibrationLeftOnET,
            R.id.vibrationLeftOffET,
            R.id.vibrationRightOnET,
            R.id.vibrationRightOffET
    };

    private ImageButton backButton;
    private ImageButton onePlayerMode;
    private ImageButton twoPlayerMode;

    private ArrayList<TestReceiver> testDataList = BluetoothActivity.testReceiverArrayList;

    //fixed
    private TestReceiver leftController = testDataList.get(0);
    //private TestReceiver rightController = testDataList.get(1);

    private ImageView joystick_dir_left, joystick_dir_right;

    private int layout = (R.layout.activity_itemlist_oneplayer);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();

        if (getIntent().hasExtra(ONEPLAYER)) {
            layout = getIntent().getExtras().getInt(ONEPLAYER);
            getIntent().removeExtra(ONEPLAYER);
        } else if (getIntent().hasExtra(TWOPLAYER)) {
            layout = getIntent().getExtras().getInt(TWOPLAYER);
            getIntent().removeExtra(TWOPLAYER);

        }
        setContentView(layout);
        InitializeUI();
    }

    private void InitializeUI() {
        //ImageButton
        for (int i = 0; i < buttonsLeftResources.length; i++) {
            buttons_left[i] = findViewById(buttonsLeftResources[i]);
        }
        for (int i = 0; i < buttons_right.length; i++) {
            buttons_right[i] = findViewById(buttonsRightResources[i]);
        }

        //EditText
        for (int i = 0; i < vibration.length; i++)
            vibration[i] = findViewById(vibrationResources[i]);

        onePlayerMode = findViewById(R.id.oneplayer_button);
        twoPlayerMode = findViewById(R.id.twoplayer_button);

        backButton = findViewById(R.id.backbutton_itemlist);

        //Button
        leftVibration = findViewById(R.id.vibrationButtonLeft);
        rightVibration = findViewById(R.id.vibrationButtonRight);

        //ImageView
        joystick_dir_left = findViewById(R.id.joystick_dir_left);
        joystick_dir_right = findViewById(R.id.joystick_dir_right);
        //mThread.start();

        onePlayerMode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //mThread.interrupt();
                layout = (R.layout.activity_itemlist_oneplayer);
                Intent intent = getIntent();
                intent.putExtra(ONEPLAYER, R.layout.activity_itemlist_oneplayer);

                leftController.setJMode(JoystickMode.JOYSTICK_UP);
                //rightController.setJMode((JoystickMode.JOYSTICK_RIGHT));

                finish();
                startActivity(intent);
            }
        });

        twoPlayerMode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //mThread.interrupt();
                layout = (R.layout.activity_itemlist_twoplayer);

                leftController.setJMode(JoystickMode.JOYSTICK_RIGHT);
                //rightController.setJMode((JoystickMode.JOYSTICK_RIGHT));

                Intent intent = getIntent();
                intent.putExtra(TWOPLAYER, R.layout.activity_itemlist_twoplayer);

                finish();
                startActivity(intent);

            }
        });

        errorSaver = new ErrorSaver(false);
        errorSaver.setChangeListener(new ErrorSaver.ChangeListener() {
            @Override
            public void onChange() {
                BluetoothActivity.errorState=true;
                finishActivity(BluetoothActivity.class, R.anim.slide_in_left, R.anim.slide_out_right);
                errorSaver.setErrorState(false);
            }
        });

        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setVibration();
        setHandlerLeft();
        setHandlerRight();
    }

    private void setVibration() {

        leftVibration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //set max. Value to 255
                byte data[] = getData(0, 1);
                try {
                    writeVibrationValues(leftController, data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        rightVibration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
               /* byte data[] = getData(2, 3);
                try {
                  //  writeVibrationValues(rightController, data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                */
            }
        });
    }

    private byte[] getData(int start, int end) {

        byte[] data = new byte[3];
        data[0] = 'V';
        for (int i = start; i <= end; i++) {
            if (!vibration[i].getText().toString().equals("")) {
                if (Long.valueOf(vibration[i].getText().toString()) > VIBRATION_MAX) {
                    vibration[i].setText(String.valueOf(VIBRATION_MAX));
                } else if (Long.valueOf(vibration[i].getText().toString()) < VIBRATION_MIN) {
                    vibration[i].setText(String.valueOf(VIBRATION_MIN));
                }
                data[i - (start - 1)] = StringToBytes(vibration[i].getText().toString());
            }
        }
        return data;
    }

    private byte StringToBytes(String text) {
        return (byte) Integer.parseInt(text);
    }

    private void writeVibrationValues(TestReceiver controller, byte[] data) throws IOException {
        controller.write(data);
    }

    private synchronized void setHandlerLeft() {
        mHandler_left = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {

                int x = msg.arg1;
                int y = msg.arg2;

                int[] buttons = (int[]) msg.obj;

                for (int i = 0; i < buttons_left.length; i++) {
                    if (buttons[i] == 1) {
                        buttons_left[i].setBackgroundResource(buttonsDrawable[i]);
                    } else {
                        buttons_left[i].setBackgroundResource(buttonsDrawable[i + buttons_left.length]);
                    }
                }

                joystick_dir_left.setX(joystick_dir_left.getX() + x);

                joystick_dir_left.setY(joystick_dir_left.getY() + y);

                return false;
            }
        });
    }

    private synchronized void setHandlerRight() {
        mHandler_right = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {

                int x = msg.arg1;
                int y = msg.arg2;

                int[] buttons = (int[]) msg.obj;

                for (int i = 0; i < buttons_right.length; i++) {
                    if (buttons[i] == 1) {
                        buttons_right[i].setBackgroundResource(buttonsDrawable[i]);
                    } else {
                        buttons_right[i].setBackgroundResource(buttonsDrawable[i + buttons_right.length]);
                    }
                }

                joystick_dir_right.setX(joystick_dir_right.getX() + x);

                joystick_dir_right.setY(joystick_dir_right.getY() + y);


                return false;

            }
        });
    }

    @Override
    public void onBackPressed() {
        BluetoothActivity.socketState = true;
        finishActivity(BluetoothActivity.class, R.anim.slide_in_left, R.anim.slide_out_right);
        //mThread.interrupt();
    }

    public void finishActivity(Class classPicked, int resource1, int resource2) {
        Intent openActivity = new Intent(ItemListActivity.this, classPicked);
        startActivity(openActivity);
        overridePendingTransition(resource1, resource2);
        finish();
    }
}