package com.example.ashish.xtension;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import static com.example.ashish.xtension.NotificationCreate.CHANNEL_ID;

public class ConnectService extends Service {


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        //open BluetoothActivity if Notification is clicked
        Intent notificationIntent = new Intent(this, ServiceActivity.class);

        //PendingIntent to put it the notification
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("XBlue Service")
                .setContentText("Running...")
                .setSmallIcon(R.drawable.logo_controller)
                .setContentIntent(pendingIntent)
                .build();

        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);

        startForeground(1, notification);
        //stop Service when background Thread is done: stopSelf();

        startThread();

        return START_STICKY;
    }


    public void startThread() {

        SocketProvider unitySocket = null;
        unitySocket = new SocketProvider(5160);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
