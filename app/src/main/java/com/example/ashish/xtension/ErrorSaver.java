package com.example.ashish.xtension;


public class ErrorSaver {

    private boolean errorState;
    private ChangeListener mListener = null;

    public ErrorSaver(boolean errorState) {
        this.errorState = errorState;
    }

    public ChangeListener getListener() {
        return mListener;
    }

    public void setErrorState(boolean errorState) {
        this.errorState = errorState;
        if (mListener != null && isErrorState()) mListener.onChange();
    }

    public boolean isErrorState() {
        return this.errorState;
    }

    public void setChangeListener(ChangeListener mListener) {
        this.mListener = mListener;
    }

    public interface ChangeListener {
        void onChange();
    }

}
