package com.example.ashish.xtension;

import android.util.Log;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketProvider extends Thread {

    public static final String SOCKETPROVIDERTAG = "SocketProvider";
    public static byte[] mBuffer = new byte[1024];
    public static int numBytes = 0;
    public static Socket unitySocket;
    private ServerSocket serverSocket;

    private int i = 0;

    private SocketProvider socketProvider = this;


    public SocketProvider(int port) {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Log.d(SOCKETPROVIDERTAG, "Constructor");
        this.start();
    }


    @Override
    public void run() {
        try {
            unitySocket = serverSocket.accept();

            Log.d(SOCKETPROVIDERTAG, "Run");


            while(!isInterrupted()){
                sendData(mBuffer, 0, numBytes);
            }


        } catch (IOException e) {
            e.printStackTrace();
            close();
        }

    }

    public void close() {

        try {
            unitySocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void sendData(byte[] buffer, int offset, int numBytes) throws IOException {

        unitySocket.getOutputStream().write(buffer, offset, numBytes);
        unitySocket.getOutputStream().flush();

    }

    private void sendData(int numBytes) throws IOException {

        unitySocket.getOutputStream().write(numBytes);
        unitySocket.getOutputStream().flush();

    }


}
