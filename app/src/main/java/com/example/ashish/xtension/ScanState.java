package com.example.ashish.xtension;

public enum ScanState {
    SCAN_FINISHED,
    SCAN_START,
    SCAN_STOP
}
