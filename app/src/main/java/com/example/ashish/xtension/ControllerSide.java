package com.example.ashish.xtension;


public enum ControllerSide {
    LEFT, RIGHT;
    public ControllerSide other(){
        return (this == LEFT) ? RIGHT : LEFT;
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }

}
