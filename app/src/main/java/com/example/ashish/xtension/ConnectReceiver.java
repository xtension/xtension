package com.example.ashish.xtension;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectReceiver extends Receiver {

    private byte[] mBuffer = new byte[1024]; // mBuffer store for the strea
    private int numBytes = 0;


    public ConnectReceiver(BluetoothSocket socket, ControllerSide side) {
        super(socket, side);
        Log.d("ConnectReceiver", "Constructor");
    }

    @Override
    public void run() {
        try {
            // ois = new ObjectInputStream(this.unitySocket.getInputStream());
            while (!this.isInterrupted()) {
                //read from the buffer
                // Log.d("Sending", numBytes +"");
                numBytes = mInStream.read(mBuffer, 0, 1024);
                if (SocketProvider.unitySocket != null) {
                    SocketProvider.numBytes = numBytes;
                    SocketProvider.mBuffer = mBuffer;
                }
                Log.d("Socket", java.util.Arrays.toString(mBuffer));

            }
        } catch (IOException e) {
            e.printStackTrace();
            cancel();
        }

        Log.d("Status", "Out of run");

    }


    @Override
    protected void cancel() {
        Log.d("ConnectReceiver", "Cancelled.");
        super.cancel();

    }
}
